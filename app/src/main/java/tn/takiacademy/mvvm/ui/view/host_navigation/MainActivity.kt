package tn.takiacademy.mvvm.ui.view.host_navigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import tn.takiacademy.mvvm.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)



    }
}