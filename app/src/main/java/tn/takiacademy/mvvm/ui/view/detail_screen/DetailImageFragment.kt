package tn.takiacademy.mvvm.ui.view.detail_screen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import tn.takiacademy.mvvm.databinding.FragmentDetailImageBinding


class DetailImageFragment : Fragment() {
    lateinit var binding: FragmentDetailImageBinding
    val args : DetailImageFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailImageBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imageClass = args.imageClass
        Glide.with(this).load(args.imageClass.urls.regular).into(binding.imagePrincipal)
        Glide.with(this).load(args.imageClass.user.profile_image.large).into(binding.imageStorie)
    }


}