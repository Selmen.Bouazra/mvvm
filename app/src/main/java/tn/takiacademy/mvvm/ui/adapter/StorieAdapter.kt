package tn.takiacademy.mvvm.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import tn.takiacademy.mvvm.databinding.StorieItemBinding
import tn.takiacademy.mvvm.model.entity.ImageItem


class StorieAdapter : RecyclerView.Adapter<StorieAdapter.ViewHolder>(){
    lateinit var onStorieClick: OnStorieClick
    var imageList:List<ImageItem> = emptyList()
    private lateinit var context : Context
    class ViewHolder(var binding: StorieItemBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val binding = StorieItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.storiesUserName.text = imageList[position].user.first_name
        Glide.with(context).load(imageList[position].user.profile_image.large).into(holder.binding.imageStories)
        holder.binding.storie.setOnClickListener {

            onStorieClick.onOnStorieClicked(imageList[position].urls.regular)
        }

    }

    override fun getItemCount(): Int = imageList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newList: List<ImageItem> ) {
        imageList = newList
        notifyDataSetChanged()
    }
}