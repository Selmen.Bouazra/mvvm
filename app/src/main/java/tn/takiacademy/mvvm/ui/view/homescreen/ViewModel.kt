package tn.takiacademy.mvvm.ui.view.homescreen

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import tn.takiacademy.mvvm.model.entity.ImageItem
import tn.takiacademy.mvvm.model.local.ImageDataBase
import tn.takiacademy.mvvm.model.local.LocalRepository
import tn.takiacademy.mvvm.model.remote.Builder
import tn.takiacademy.mvvm.model.remote.RemoteRepositoryImp
import tn.takiacademy.mvvm.model.remote.ServiceAPI
import java.util.*


class ViewModel(application: Application): AndroidViewModel(application){

    private val OffLigneData: LiveData<List<ImageItem>>
    private val localRepository : LocalRepository
    private var remoteRepositoryImp = RemoteRepositoryImp(
        Builder.get().create(
            ServiceAPI::class.java
        )
    )

    init {

        //get image from api unsplash
        @RequiresApi(Build.VERSION_CODES.M)
        if (isOnline(application)) {
         getImagesAPI()
        }


        val imageDao = ImageDataBase.getDatabase(application).imageDao()
        localRepository = LocalRepository(imageDao)
        OffLigneData = localRepository.imageLocalList

    }


    //insert data to dataBase
    fun addImageToDatabase(imageList: List<ImageItem>){

        viewModelScope.launch (Dispatchers.IO){
            for (image in imageList) {
                val image = ImageItem(
                    image.id,
                    image.alt_description,
                    image.created_at,
                    image.description,
                    image.updated_at,
                    image.urls,
                    image.user
                )
                localRepository.addUser(image)
            }
        }
    }

    fun getDataRoom():LiveData<List<ImageItem>> = OffLigneData


    // get from unsplash API
    fun getImagesAPI() = viewModelScope.launch {
        val result = remoteRepositoryImp.getPhoto()
        if (result.isSuccessful) {
            if (result.body() != null) {
                // set Api data to DB
                addImageToDatabase(result.body()!!)
            }
        }
        else {
            Log.i("errorMsg", result.message())
        }
    }



    fun search(text: String) : List<ImageItem> {
        val imagelists : List<ImageItem>? = OffLigneData.value
        val result : MutableList<ImageItem> = mutableListOf()
        imagelists?.forEach {
            if (it.user.first_name.uppercase(Locale.getDefault()).startsWith(text.uppercase(
                    Locale.getDefault()
                ))){
                result.add(it)
            }
        }
        return result
    }


    //check internet
    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }
}


