package tn.takiacademy.mvvm.ui.view.homescreen


import android.accounts.NetworkErrorException
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import tn.takiacademy.mvvm.ui.adapter.HomeAdapter
import tn.takiacademy.mvvm.ui.adapter.OnListItemClick
import tn.takiacademy.mvvm.ui.adapter.OnStorieClick
import tn.takiacademy.mvvm.ui.adapter.StorieAdapter
import tn.takiacademy.mvvm.databinding.FragmentHomeScreenBinding
import tn.takiacademy.mvvm.model.entity.ImageItem
import tn.takiacademy.mvvm.ui.view.stories_topup.StorieFragment


class HomeScreenFragment : Fragment(), OnListItemClick, OnStorieClick {
    private val storieAdapter : StorieAdapter = StorieAdapter()
    private val homeAdapter: HomeAdapter = HomeAdapter()
    private lateinit var homeScreenViewModel: ViewModel
    lateinit var binding : FragmentHomeScreenBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding  = FragmentHomeScreenBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeScreenViewModel = ViewModelProvider(this)[ViewModel::class.java]

        homeAdapter.onListItemClick = this
        storieAdapter.onStorieClick = this


        // set data to adapter
         homeScreenViewModel.getDataRoom().observe(viewLifecycleOwner,{
             binding.homeRecycleView.adapter = homeAdapter
             binding.storiesRecycleView.adapter = storieAdapter
             homeAdapter.setData(it)
             storieAdapter.setData(it)
        })


        //search
        binding.editText.doOnTextChanged { text, start, before, count ->
            homeAdapter.setData(homeScreenViewModel.search(text.toString()))
        }
    }

    override fun onItemClick(image: ImageItem) {
        val action = HomeScreenFragmentDirections.actionHomeScreenFragmentToDetailImageFragment(image)
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onOnStorieClicked(image : String) {
        val storieFragment  = StorieFragment(image)
        storieFragment.show(childFragmentManager, "StorieFragment")
    }
}