package tn.takiacademy.mvvm.ui.view.stories_topup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import tn.takiacademy.mvvm.databinding.FragmentStorieBinding


class StorieFragment(var image : String) : DialogFragment() {
    lateinit var binding : FragmentStorieBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStorieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this).load(image).into(binding.imageStorie)
    }

}