package tn.takiacademy.mvvm.ui.adapter

import tn.takiacademy.mvvm.model.entity.ImageItem

interface OnListItemClick {
    fun onItemClick(image: ImageItem)
}