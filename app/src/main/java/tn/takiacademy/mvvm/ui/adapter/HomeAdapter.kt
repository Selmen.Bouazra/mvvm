package tn.takiacademy.mvvm.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import tn.takiacademy.mvvm.databinding.HomeItemBinding
import tn.takiacademy.mvvm.model.entity.ImageItem


class HomeAdapter :RecyclerView.Adapter<HomeAdapter.ViewHolder>(){
    var onListItemClick: OnListItemClick? =null
    var imageList:List<ImageItem> = emptyList()
    private lateinit var context : Context

    class ViewHolder(var binding: HomeItemBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val binding = HomeItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.userName.text = imageList[position].user.name
        holder.binding.creationDate.text = imageList[position].created_at
        Glide.with(context).load(imageList[position].urls.regular).into(holder.binding.userImage)

        holder.binding.userImage.setOnClickListener {
            onListItemClick?.onItemClick(imageList[position])
        }
    }

    override fun getItemCount(): Int = imageList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newList: List<ImageItem> ) {
        imageList = newList
        notifyDataSetChanged()
    }

    fun get():List<ImageItem> = imageList
}