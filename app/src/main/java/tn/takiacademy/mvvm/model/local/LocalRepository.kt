package tn.takiacademy.mvvm.model.local

import androidx.lifecycle.LiveData
import tn.takiacademy.mvvm.model.entity.ImageItem


class LocalRepository(private val imageDao: ImageDao) {

    val imageLocalList: LiveData<List<ImageItem>> = imageDao.getImageList()

    suspend fun addUser(image: ImageItem){
        imageDao.addImage(image)
    }

}