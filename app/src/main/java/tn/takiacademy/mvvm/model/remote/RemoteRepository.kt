package tn.takiacademy.mvvm.model.remote

import retrofit2.Response
import tn.takiacademy.mvvm.model.entity.ImageItem

interface RemoteRepository {

    suspend fun getPhoto(): Response<List<ImageItem>>
}