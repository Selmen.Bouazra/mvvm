package tn.takiacademy.mvvm.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Builder {
    companion object {
        private const val url = "https://api.unsplash.com/"
        fun get(): Retrofit {
            return Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create()).build()
        }
    }
}


