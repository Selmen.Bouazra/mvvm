package tn.takiacademy.mvvm.model.remote

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import tn.takiacademy.mvvm.model.entity.ImageItem


interface ServiceAPI {

    @GET("photos/random/?count=14")
    @Headers("Authorization: Client-ID U8E1f1_pEX6rPKsW6QfuGT5SmQhgQ7GJYVPfOqvBlo8")
    suspend fun getPhoto(): Response<List<ImageItem>>
}
