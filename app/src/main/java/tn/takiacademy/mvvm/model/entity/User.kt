package tn.takiacademy.mvvm.model.entity

import androidx.room.Embedded

data class User(
    val first_name: String = "first_name",
    val name: String? = "name",
    @Embedded
    val profile_image: ProfileImage,
    val username: String?
)