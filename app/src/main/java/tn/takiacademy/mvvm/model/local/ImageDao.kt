package tn.takiacademy.mvvm.model.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import tn.takiacademy.mvvm.model.entity.ImageItem


@Dao
interface ImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addImage(imageItem: ImageItem)

    @Query("SELECT * FROM image_table")
    fun getImageList():LiveData<List<ImageItem>>
}