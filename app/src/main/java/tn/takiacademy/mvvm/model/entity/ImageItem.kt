package tn.takiacademy.mvvm.model.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "image_table")
data class ImageItem(
    @PrimaryKey
    val id: String,
    val alt_description: String?,
    val created_at: String?,
    val description: String?,
    val updated_at: String?,
    @Embedded
    val urls: Urls,
    @Embedded
    val user: User,
):Serializable