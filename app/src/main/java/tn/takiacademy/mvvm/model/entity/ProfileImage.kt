package tn.takiacademy.mvvm.model.entity

data class ProfileImage(
    val large: String,
    val medium: String,
    val small: String
)